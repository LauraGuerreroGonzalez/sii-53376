//Laura Guerrero González

#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>

int main(int argc, char **argv)
{
//FILE *pipe;
char buffer[128];
if (mkfifo("/tmp/pipe", 0777)!=0)
	printf ("Error al crear la tuberia\n");

int mipipe=open("/tmp/pipe", O_RDONLY);
	if(mipipe<0)
        {
                perror("error en open");
                return 1;
        }
while (read(mipipe, buffer, 40))
{
	printf("%s",buffer);
}

if (close (mipipe)!=0)
        {
                perror("error al cerrar");
                return 1;
        }

int r=unlink("/tmp/pipe");
if(r<0)
	{
               perror("error en unlink");
               return 1;
	}
return 0;

}
