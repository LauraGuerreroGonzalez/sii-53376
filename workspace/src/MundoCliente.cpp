// Laura Guerrero González
// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoCliente.h"
#include "glut.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <sys/mman.h>
#include <unistd.h>
#include <fcntl.h>
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{
	munmap(org, sizeof(datosm));//Desproyectamos

}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();
	//Dibujamos los disparos
	for(i=0;i<disparos1.size();i++)
		disparos1[i].Dibuja();
	for(i=0;i<disparos2.size();i++)
		disparos2[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	//esfera.Dibuja();
	//Multiplicamos las esfera
	for(i=0;i<esferas.size();i++)
		esferas[i].Dibuja();


	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{
//	t1 = clock();
//	time = (double(t1-t0)*10/CLOCKS_PER_SEC);	
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	//esfera.Mueve(0.025f);
	int i;
	int j;
	//Con este for conseguimos que todas las esferas se muevan
	for(i=0;i<esferas.size();i++)
		esferas[i].Mueve(0.025f);
	//Con este bucle movemos los disparos de los dos jugadores
	for(i=0;i<disparos1.size();i++)
		disparos1[i].Mueve(0.025f);
	for(i=0;i<disparos2.size();i++)
		disparos2[i].Mueve(0.025f);
	//Con este for conseguimos que puedan rebotar todas las esferas
	//bottar for(j=0;j<esferas.size();j++)
	//borrar	paredes[i].Rebota(esferas[j]);
		
	for(i=0;i<paredes.size();i++)
	{
		//paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
		for(j=0;j<esferas.size();j++)
 	               paredes[i].Rebota(esferas[j]);

	}

	//jugador1.Rebota(esfera);
	//jugador2.Rebota(esfera);
	//Con este bucle las esferas rebotan con los jugadores
	for(int i=0;i<esferas.size();i++)
	{
		jugador1.Rebota(esferas[i]);
		jugador2.Rebota(esferas[i]);
	}
	for(int i=0;i<esferas.size();i++){
	if(fondo_izq.Rebota(esferas[i]))
	{
		esferas[i].centro.x=0;
		esferas[i].centro.y=rand()/(float)RAND_MAX;
		esferas[i].velocidad.x=2+2*rand()/(float)RAND_MAX;
		esferas[i].velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;
	}
	}

	for(int i=0;i<esferas.size();i++){
	if(fondo_dcho.Rebota(esferas[i]))
	{
		esferas[i].centro.x=0;
		esferas[i].centro.y=rand()/(float)RAND_MAX;
		esferas[i].velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esferas[i].velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;
	}
	}
	//Para controlar el avance el juego se crea un contador que controle el numero de rebotes
	if((jugador1.Rebota(esferas[0]))||(jugador2.Rebota(esferas[0]))){
			botar++;
	}
	for(i=0;i<esferas.size();i++){
	if((esferas.size()==1)&&(botar>3)){
		Esfera e;
		e.radio=1;
		e.centro.x=0;
		e.centro.y=rand()/(float)RAND_MAX;
		e.velocidad.x=2+2*rand()/(float)RAND_MAX;
		e.velocidad.y=2+2*rand()/(float)RAND_MAX;
		esferas.push_back(e);
		botar=0;
		}
	}
	//Disparos jugador 1 contra el jugador 2
	for(int i=0;i<disparos1.size();i++)
		{
		if(jugador2.Rebota(disparos1[i]))
		{
			if((jugador2.y2-jugador2.y1)>1.5){
			jugador2.y2=jugador2.y2-0.5;
			jugador2.y1=jugador2.y1+0.5;}
			disparos1.erase(disparos1.begin(),disparos1.begin()+disparos1.size());	
		}
		}
	//Disparos jugador 2 contra el jugador 1
	for(int i=0;i<disparos2.size();i++)
		{
		if(jugador1.Rebota(disparos2[i]))
		{
			if((jugador1.y2-jugador1.y1)>1.5){
			jugador1.y2=jugador1.y2-0.5;
			jugador1.y1=jugador1.y1+0.5;}
			disparos2.erase(disparos2.begin(),disparos2.begin()+disparos2.size());	
		}

		}

	//Si alguno de los dos jugadores llega a tres puntos se sale del juego
	if((puntos1==3)||(puntos2==3))
		exit(0);
	//Actualizamos campos del atrib datos memoria compartida
	switch(datosmpunt->accion){
		case 1: OnKeyboardDown('w',0,0); break;
		case -1: OnKeyboardDown('s',0,0); break;
		case 0: break;
	}
	//Intento que que la segunda raqueta juegue en modo automatico si pasan mas de 10 segundos
	/*if (time>10){
		switch(datosmpunt->accion2){
			case 1: jugador2.velocidad.y=4; break;
			case -1: jugador2.velocidad.y=-4; break;
			case 0: break;
		}
	}*/
	datosmpunt->esfera=esferas[0];//devuelve posicion de la primera esfera
	datosmpunt->raqueta1=jugador1; //devuelve posición de raqueta1
	datosmpunt->raqueta2=jugador2; //devuelve posición de raqueta1
/*	char cad[40];

	sprintf(cad,"Jugador 1 lleva un total de %d puntos\n", puntos1);
	write(tuberia,cad,sizeof(cad));
	sprintf(cad,"Jugador 2 lleva un total de %d puntos\n", puntos2);
	write(tuberia,cad,sizeof(cad));*/
	char cad[200];
	//read(fifo_servidor_cliente,cad,sizeof(cad));
	s_comunicacion.Receive(cad, sizeof(cad));
	sscanf(cad,"%f %f %f %f %f %f %f %f %f %f %d %d", 
		&esferas[0].centro.x,&esferas[0].centro.y, 
		&jugador1.x1,&jugador1.y1,&jugador1.x2,&jugador1.y2, 
		&jugador2.x1,&jugador2.y1,&jugador2.x2,&jugador2.y2, 
		&puntos1, &puntos2); 


}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
	char tecla[]="0";
	switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
	case 's':sprintf(tecla,"s");break;
	case 'w':sprintf(tecla,"w"); break;
	case 'l':sprintf(tecla,"l");break;
	case 'o':sprintf(tecla,"o");break;
/*	case 'c':
		if(disparos1.size()==0){
		Esfera e;
		e.radio=0.2;
		e.centro.x=jugador1.x1+0.6;
		e.centro.y=jugador1.y1+(jugador1.y2-jugador1.y1)/2;
		e.velocidad.y=0;
		e.velocidad.x=3;
		e.rojo=255;
		e.verde=0;
		e.azul=0;
		disparos1.push_back(e);
		}
		break;
	case 'm':
		if(disparos2.size()==0){
		Esfera e;
		e.radio=0.2;
		e.centro.x=jugador2.x1-0.6;
		e.centro.y=jugador2.y1+(jugador2.y2-jugador2.y1)/2;
		e.velocidad.y=0;
		e.velocidad.x=-3;
		e.rojo=0;
		e.verde=255;
		e.azul=0;
		disparos2.push_back(e);
		}
		break;*/

	}
	s_comunicacion.Send(tecla,sizeof(tecla));

}

void CMundo::Init()
{
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izqd 
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
	//crear la primera esfera

	Esfera e;
	e.radio=0.5;
	e.centro.x=0;
	e.centro.y=rand()/(float)RAND_MAX;
	e.velocidad.x=2+2*rand()/(float)RAND_MAX;
	e.velocidad.y=2+2*rand()/(float)RAND_MAX;
	esferas.push_back(e);
	//Tuberia
	//int tuberia = open ("tuberia",O_WRONLY);

	//Tuberia teclas
	//CONEXION CON SERVIDOR
	char ip[]="127.0.0.1";
	char nombre[50];
	printf("Introduzca su nombre:\n");
	scanf("%s",nombre);
	s_comunicacion.Connect(ip,8000);

//ENVIO DEL NOMBRE AL SERVIDOR

	s_comunicacion.Send(nombre,sizeof(nombre));


	//Datos Memoria Compartida
	int fd=open("DatosComp.txt",O_RDWR|O_CREAT|O_TRUNC,0777);
//	if(fd<0)
//		perror("Error al abrir el fichero");
	write (fd,&datosm,sizeof(datosm));
	//Convierte a char 
	org=(char*)mmap(NULL,sizeof(datosm),PROT_WRITE|PROT_READ,MAP_SHARED,fd,0);


	close (fd);
	datosmpunt=(DatosMemCompartida*)org;
	datosmpunt->accion=0;
	//datosmpunt->accion2=0;

}
